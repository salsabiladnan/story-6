from django.urls import path
from .views import books

app_name = 'story8'

urlpatterns = [
    path('', books, name = 'books'),
]