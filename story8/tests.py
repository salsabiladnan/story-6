from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import books

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Story_8_Unit_Test(TestCase):

    def test_story_8_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_story_8_using_index_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)

    def test_story_8_using_search_books_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

class Story8FunctionalTest(TestCase):
	# functional testing
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('disable-gpu')

		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		time.sleep(5)
		super(Story8FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super().tearDown()

	def test_if_input_django_output_also_django(self) :
		selenium = self.selenium

		selenium.get ('http://127.0.0.1:8000/books')
		time.sleep(5)
		input_= selenium.find_element_by_id('searchbook')

		input_.send_keys('Django')
		input_.send_keys(Keys.RETURN)
		time.sleep(4)
		table = selenium.find_element_by_css_selector('#table2')
		myTable = selenium.execute_script('return arguments[0].innerText;', table)

		self.assertIn('Django', myTable)
		time.sleep(3)
