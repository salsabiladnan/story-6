$(document).ready(function(){
    searchBook("Django");
    $("#searchbook").change(function(){
        searchBook(document.getElementById("searchbook").value);
    });
});

function searchBook(toSearch) {
    $.getJSON(`https://www.googleapis.com/books/v1/volumes?q=${toSearch}`, function(result){
        let counter = 1;
        document.getElementsByTagName("table")[0].innerHTML = 
`
<thead>
<tr>
    <th scope="col">#</th>
    <th scope="col">Picture</th>
    <th scope="col">Title</th>
    <th scope="col">Authors</th>
</tr>
</thead>
<tbody>
</tbody>
`;
        result.items.forEach(value => {
            let row = document.createElement("tr");

            let data0 = document.createElement("td");
            data0.innerText = counter;
            row.appendChild(data0);

            let dataImg = document.createElement("td");
            dataImg.innerHTML = `<img src=${value.volumeInfo.imageLinks.thumbnail}>`;
            row.appendChild(dataImg);

            let dataTitle = document.createElement("td");
            dataTitle.innerText = value.volumeInfo.title;
            row.appendChild(dataTitle);
            
            let dataAuthor = document.createElement("td");
            dataAuthor.innerText = value.volumeInfo.authors.join(", ");
            row.appendChild(dataAuthor);

            document.getElementsByTagName("tbody")[0].appendChild(row);
            ++counter;
        });
    });
}
