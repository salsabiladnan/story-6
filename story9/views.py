from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def index(request):

	template_name = None

	if request.user.is_authenticated:
		#untuk user
		template_name = 'welcome.html'

	else:
		#untuk anon
		template_name = 'welcome-login.html'

	host = request.get_host()
	port = request.get_port()

	context = {
        'host':host,
        'port':port,
    }

	return render(request, template_name, context)

def loginView(request):

	user = None

	if request.method == "POST":
		username_login = request.POST['username']
		password_login = request.POST['password']

		user = authenticate(request, username=username_login, password=password_login)

		if user is not None:
			login(request, user)
			return redirect('story9:index')
		else:
			return redirect('story9:login')
	return render(request, 'login.html')
		
# @login_required
def logoutView(request):

	if request.method == "POST":
		if request.POST['logout'] == "Yes":
			logout(request)
		

		return redirect('story9:index')
	return render(request, 'logout.html')
