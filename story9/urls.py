from django.urls import path
from .views import index, loginView, logoutView

app_name = 'story9'

urlpatterns = [
    path('', index, name = 'index'),
    path('login/', loginView, name='login'),
    path('logout/', logoutView, name='logout'),
]