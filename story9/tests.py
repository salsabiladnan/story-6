from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, loginView, logoutView

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Story_9_Unit_Test(TestCase):

    def test_welcome_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_9_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_story_9_using_loginView_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginView)

    def test_story_9_using_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_logout_url_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)

    def test_story_9_using_logoutView_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logoutView)

    def test_story_9_using_logout_template(self):
        response = Client().get('/logout/')
        self.assertTemplateUsed(response, 'logout.html')

class Story9FunctionalTest(TestCase):
	# functional testing
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('disable-gpu')

		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		time.sleep(5)
		super(Story9FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super().tearDown()

	def test_login_logout(self):
		selenium = self.selenium
        # Opening the link we want to test
		selenium.get('http://localhost:8000/')
		time.sleep(5) 
		
		selenium.find_element_by_id("login-but").click()

		username_input = selenium.find_element_by_id('username')
		username_input.send_keys('kakpewe')
		password_input = selenium.find_element_by_id('password')
		password_input.send_keys('lulusppw')

		selenium.find_element_by_id('submit-but').click()

		
		time.sleep(2)
		selenium.find_element_by_id("logout-but").click()
		
		
		selenium.find_element_by_id("logout-yes").click()
