from django import forms
from django.forms import Form

class Status_Form(forms.Form):
	pos_attrs = {
		'type': 'text',
		'class':'form-control',
		'placeholder':'Write your status here'
	}

	status = forms.CharField(required=True, widget=forms.Textarea(attrs=pos_attrs), max_length=300)