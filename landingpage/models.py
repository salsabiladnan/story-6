from django.db import models

# Create your models here.
class Status(models.Model):
	status = models.TextField(blank=False, max_length=300)
	status_time = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ('-status_time',)
