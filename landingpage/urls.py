from django.urls import path
from .views import index, add_status, about

app_name = 'landingpage'

urlpatterns = [
    path('status/', index, name = 'index'),
    path('add_status/', add_status, name='add_status'),
    path('about/', about, name = 'about'),
]
