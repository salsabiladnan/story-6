$(document).ready(function(){

  $('.panels').click(function(e) {
    e.preventDefault();

  var $this = $(this);

  if ($this.next().hasClass('show')) {
    $this.next().removeClass('show');
    $this.next().slideUp(350);
  } else {
    $this.parent().parent().find('li .content').removeClass('show');
    $this.parent().parent().find('li .content').slideUp(350);
    $this.next().toggleClass('show');
    $this.next().slideToggle(350);
  }
});
});

function darkMode() {
	document.body.style.backgroundColor = "#313131";
	document.body.style.color = "#F0ECE2";
	var accor = document.getElementsByClassName('panels');
	for(var i=0; i<accor.length; i++){
		accor[i].style.color = '#f0ece2';
		accor[i].style.backgroundColor = '#525252';
	}

	var desc = document.getElementsByClassName('content');
	for(var i=0; i<desc.length; i++){
		desc[i].style.backgroundColor = '#393232';
	}

	var card = document.getElementsByClassName('card');
	for(var i=0; i<card.length; i++){
		card[i].style.background = '#4d4545';
	}
}

function lightMode() {
	document.body.style.backgroundColor = "#f0ece2";
	document.body.style.color = "black";
	var accor = document.getElementsByClassName('panels');
	for(var i=0; i<accor.length; i++){
		accor[i].style.color = '#525252';
		accor[i].style.backgroundColor = '#ede68a';
	}

	var desc = document.getElementsByClassName('content');
	for(var i=0; i<desc.length; i++){
		desc[i].style.backgroundColor = '#f5eded';
	}

	var card = document.getElementsByClassName('card');
	for(var i=0; i<card.length; i++){
		card[i].style.background = '#dbd8e3';
	}

}