from django.test import TestCase
from django.test import Client
from django.contrib import admin
from django.urls import resolve
from django.http import HttpRequest
from .apps import LandingpageConfig
from .views import index, add_status, about
from .models import Status
from .forms import Status_Form

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class LandingPageUnitTest(TestCase):
    
	def test_status_url_is_exist(self):
	    response = Client().get('/status/')
	    self.assertEqual(response.status_code, 200)

	def test_landingpage_using_index_func(self):
	    found = resolve('/status/')
	    self.assertEqual(found.func, index)

	def test_page_is_not_exist(self):
		self.response = Client().get('wek/')
		self.assertEqual(self.response.status_code, 404)

	def test_halo_apa_kabar(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Halo, Apa Kabar?', html_response)

	def test_landingpage_use_index_template(self):
		response = Client().get('/status/')
		self.assertTemplateUsed(response, 'index.html')

	def test_about_url_is_exist(self):
	    response = Client().get('/about/')
	    self.assertEqual(response.status_code, 200)

	def test_index_have_status(self):
		# Creating a new status
		new_status = Status.objects.create(status='test')
		new_status.save()
		# Retrieving all available status
		response = Client().get('/status/')
		self.assertContains(response, 'test')

	def test_index_add_status_valid(self):
	 	response = Client().post('/add_status/', {'status':'test'})
	 	self.assertRedirects(response, '/status/')
        
	 	response2 = Client().get('/status/')
	 	self.assertContains(response2, 'test')

	

class LandingPageFunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('disable-gpu')
		
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		time.sleep(5)
		super(LandingPageFunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(LandingPageFunctionalTest, self).tearDown()

	def test_input_todo(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://127.0.0.1:8000/status')
		time.sleep(5)
		# find the form element
		status = selenium.find_element_by_id('id_status')
		submit = selenium.find_element_by_id('submit-btn')

		# Fill the form with data
		status.send_keys('test')

		# submitting the form
		submit.send_keys(Keys.RETURN)
		time.sleep(5)


	




	